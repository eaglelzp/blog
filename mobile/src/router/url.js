/**
 * 需要录入系统的菜单URI
 */
export default [
  {
    "id": 2,
    "name": "用户管理",
    "path": "",
    "type": 1,
    "children": [
      {
        "id": "2-1",
        "name": "系统用户",
        "path": "/sys/users/findAll",
        "type": 2,
        children: []
      }
    ]
  },
  {
    "id": 3,
    "name": "系统管理",
    "path": "",
    "type": 1,
    "children": [
      {
        "id": "3-1",
        "name": "权限管理",
        "path": "",
        "type": 1,
        "children": [
          {
            "id": "3-1-1",
            "name": "权限列表",
            "path": "/sys/perms/findAll",
            "type": 2,
            children: []
          },
          {
            "id": "3-1-3",
            "name": "菜单列表",
            "path": "/sys/menus/findAll",
            "type": 2,
            children: []
          }
        ]
      },
      {
        "id": "3-2",
        "name": "角色管理",
        "path": "",
        "type": 1,
        "children": [
          {
            "id": "3-2-1",
            "name": "角色列表",
            "path": "/sys/roles/findAll",
            "type": 2,
            children: []
          }
        ]
      },
      {
        "id": 3-3,
        "name": "任务管理",
        "path": "",
        "type": 1,
        "children": [
          {
            "id": "3-3-1",
            "name": "定时任务",
            "path": "/sys/task/findAll",
            "type": 2,
            "children": []
          }
        ]
      },
      {
        "id": 3-4,
        "name": "收费管理",
        "path": "",
        "type": 1,
        "children": [
          {
            "id": "3-4-1",
            "name": "收款账号",
            "path": "/sys/bank/findAll",
            "type": 2,
            "children": []
          }
        ]
      }
    ]
  },

]
