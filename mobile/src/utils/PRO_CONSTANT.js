/**
 * 客户端常量
 */
var NOTICE_INFO ={
    IS_LOADING_BOTTOM:"到底部啦~",
    IS_LOADING_EMPTY:"空空如也~",
    IS_LOADING:"正在加载",
}

export default NOTICE_INFO;
