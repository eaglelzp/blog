package com.jtw.conf.shiro.model;

import lombok.Data;

import java.io.Serializable;

/**
 * DESCRIPT:基础用户
 *
 * @author cjsky666
 * @date 2018/9/3 10:33
 */
@Data
public class BaseUser implements Serializable {
    private Long id;
    private String userName;
    private String password;
    private String salt;
    private Integer state;
}
