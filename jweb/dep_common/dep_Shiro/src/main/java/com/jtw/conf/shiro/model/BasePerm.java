package com.jtw.conf.shiro.model;

import lombok.Data;

import java.io.Serializable;

/**
 * DESCRIPT:基础权限
 *
 * @author cjsky666
 * @date 2018/9/3 10:34
 */
@Data
public class BasePerm implements Serializable {
    private Integer id;
    private String name;
    private Integer type;
    private String url;
    private String desc;
    private String httpMethod;
    private String methodName;
    private String className;
    private Boolean open;
}
