package com.jtw.constant;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2019/3/28 11:46
 */
public enum  IM_CMD {
    HEARTBEAT_REQ("10001","心跳请求"),
    HANDSHAKE_REQ("10002","握手请求"),
    HANDSHAKE_RESP("10003","握手响应"),
    JOIN_GROUP_REQ("10004","进入群组请求"),
    JOIN_GROUP_RESP("10005","进入群组响应"),
    JOIN_GROUP_NTY("10006","进入群组通知"),
    LEAVE_GROUP_NTY("10007","离开群组通知"),
    P2P_CHAT_REQ("10008","点对点聊天（私聊）请求"),
    P2P_CHAT_NTY("10009","点对点聊天（私聊）通知"),
    GROUP_CHAT_REQ("10010","群聊请求"),
    GROUP_CHAT_NTY("10011","群聊通知"),
    P2P_QUERY_CHAT_RECORD_REQ("10012","获取p2p聊天记录数据-请求"),
    RUN_JS_NTY("10014","运行js脚本"),
    CLOSE_PAGE("10015","让客户端关闭当前页面（只作用于WEB端）"),
    MSG_TIP_REQ("10016","系统通知请求"),
    MSG_TIP_NTY("10017","系统通知响应"),
    PAGE_ONLINE_REQ("10018","分页获取在线用户请求"),
    PAGE_ONLINE_RESP("10019","分页获取在线用户响应"),
    UPDATE_TOKEN_REQ("10020","更新token"),
    UPDATE_TOKEN_RESP("10021","更新token响应"),
    UNSEND_MSG_REQ("10022","撤回消息"),
    UNSEND_MSG_NTY("10023","撤回消息通知"),
    USER_ACTION_LOG_REQ("10024","用户动作日志"),
    P2P_ALREADY_READ_REQ("10025","我告诉服务器，张三发给我的私聊消息已读"),
    P2P_ALREADY_READ_NTY("10026","服务器告诉张三，张三发给李四的私聊，李四已经阅读（暂不实现，因为此需求有争议）"),
    P2P_NOT_READ_COUNT_REQ("10027","查询未读私聊消息数请求"),
    P2P_NOT_READ_COUNT_RESP("10028","查询未读私聊消息数响应"),
    ALL_CHAT_ROMM_REQ("10029","获取客服列表指令"),
    ALL_CHAT_ROMM_RESP("10030","响应客服列表指令"),
    LEAVE_GROUP_REQ("10031","离开群组请求"),
    All_ONLINE_REQ("10032","所有在线用户请求"),
    All_ONLINE_RESP("10033","所有在线用户响应"),
    USER_ONLINE_NTY("10034","上线通知"),
    USER_OFFLINE_NTY("10035","离线通知"),
    ;
    private String type;
    private String descript;

    IM_CMD(String type,String descript) {
        this.type=type;
        this.descript=descript;
    }
    public String getType() {
        return type;
    }
    public String getDescript() {
        return descript;
    }
}
