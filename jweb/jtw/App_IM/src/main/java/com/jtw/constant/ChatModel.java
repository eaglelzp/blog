package com.jtw.constant;

import lombok.Data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * DESCRIPT: IM列表树模型
 *
 * @author cjsky666
 * @date 2019/4/11 16:51
 */
@Data
public class ChatModel {
    /**
     * 用户id
     */
    private String sid;
    /**
     * 用户昵称
     */
    private String sname;
    /**
     * 在线状态
     */
    private int state = 1;
    /**
     * 最后发送时间 时间戳
     */
    private Long sendTime = new Date().getTime();
    /**
     * type 类型 默认为1 P2P类型，2为group
     */
    private Integer type=1;
    /**
     *消息条数
     */
    private Integer badge = 0;
    /**
     * 消息记录数
     */
    private List<IM_Message> logs=new ArrayList<>();
}
