package com.jtw.common.util;

import java.io.IOException;
import java.util.Properties;

public class PropertiesUtils {
	
	private static Properties proper = new Properties();
	
	static {
		new PropertiesUtils("../classes/system/sysconfig.properties");
	}
	
	public PropertiesUtils(String fileName) {
		try {
			java.io.InputStream in = getClass().getResourceAsStream((new StringBuilder()).append("/").append(fileName).toString());
            proper.load(in);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static String getProperty(String key) {
		return proper.getProperty(key);
	}
	
	public static void main(String[] args) {
		System.out.println(PropertiesUtils.getProperty("default_sms_channel"));
	}
}
