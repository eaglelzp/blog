package com.jtw.common.bean.enums;

import org.springframework.http.MediaType;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2018/11/19 16:58
 */
public enum  HeadImageType {

    IMAGE_PNG(MediaType.IMAGE_PNG_VALUE),
    IMAGE_JPEG(MediaType.IMAGE_JPEG_VALUE),
    IMAGE_ICON("image/x-icon"),
    IMAGE_BMP("image/bmp"),
    IMAGE_MICRO_ICON("mage/vnd.microsoft.icon")
    ;
    private String value;
    HeadImageType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
