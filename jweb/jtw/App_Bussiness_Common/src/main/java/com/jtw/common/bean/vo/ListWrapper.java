package com.jtw.common.bean.vo;

import lombok.Data;

/**
 * 序列化/反序列化对象包装类
 * 专为基于 Protostuff 进行序列化/反序列化而定义。
 * Protostuff 是基于POJO进行序列化和反序列化操作。
 * 如果需要进行序列化/反序列化的对象不知道其类型，不能进行序列化/反序列化；
 * 比如Map、List、String、Enum等是不能进行正确的序列化/反序列化。
 * @author cjsky666
 * @date 2019/1/9 14:39
 */
@Data
public class ListWrapper<T> {
    private T data;
    public static <T> ListWrapper<T> builder(T data) {
        ListWrapper<T> wrapper = new ListWrapper<>();
        wrapper.setData(data);
        return wrapper;
    }

}
