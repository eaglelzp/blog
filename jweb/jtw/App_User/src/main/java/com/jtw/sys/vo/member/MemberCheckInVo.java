package com.jtw.sys.vo.member;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * DESCRIPT: 会员签到记录详情
 *
 * @author cjsky666
 * @date 2019/7/8 16:33
 */
@Data
public class MemberCheckInVo implements Serializable {
    private static final long serialVersionUID = 3207522366332068054L;
    private Integer id;
    private String userName;
    @JsonFormat(pattern="yyyy/MM/dd")
    private Date checkInDate;
    private Integer checkInRewardBalance;
    private Integer state;
    private String confirmInfo;
}
