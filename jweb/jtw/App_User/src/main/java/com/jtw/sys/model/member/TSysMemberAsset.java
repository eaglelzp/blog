package com.jtw.sys.model.member;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * DESCRIPT:会员资产表
 *
 * @author cjsky666
 * @date 2019/7/18 10:51
 */
@Data
@Table(name = "`t_sys_member_asset`")
public class TSysMemberAsset implements Serializable {
    private static final long serialVersionUID = -7041894063299647264L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID() from dual")
    private Long id;
    private Integer balance;
    private Integer freezeBalance;
    @JsonFormat(pattern="yyyy/MM/dd HH:mm:ss",timezone="GMT+8")
    private Date createTime;
    @JsonFormat(pattern="yyyy/MM/dd HH:mm:ss",timezone="GMT+8")
    private Date updateTime;
}
