//package com.jtw.mqconf;
//
//import org.springframework.amqp.core.AmqpTemplate;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.redis.core.RedisTemplate;
//import org.springframework.stereotype.Component;
//
//import java.util.Date;
//
///**
// * @author chenjian
// * @Date 2018-08-21 14:56
// * @desc 这是一个队列组件
// */
//
//@Component
//public class HelloSender {
//    @Autowired
//    private AmqpTemplate amqpTemplate;
//    @Autowired
//    private RedisTemplate redisTemplate;
//    public void send(int i){
//        String context =i+"==========="+new Date();
//        System.out.println("正在发送"+context);
//        redisTemplate.opsForValue().set(i+"",context);
//        amqpTemplate.convertAndSend("hello",context);
//    }
//}
