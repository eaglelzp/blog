package com.jtw.sys.service.menu;

import com.jtw.common.baseService.BaseService;
import com.jtw.sys.model.menu.TSysMenu;
import com.jtw.sys.vo.menu.SysMenuMarkVo;
import com.jtw.sys.vo.menu.SysMenuRoleMarkModifyReq;
import com.jtw.sys.vo.menu.SysMenuTreeVo;

import java.util.List;

/**
 * DESCRIPT:系统管理后台菜单服务接口
 *
 * @author cjsky666
 * @date 2018/10/10 10:36
 */
public interface SysMenuService extends BaseService<TSysMenu> {
    /**
     * 根据角色id查询用户的管理菜单
     * @param roleId
     * @return
     */
    List<SysMenuMarkVo> getAllSysMenuTreeMarkByRoleId(Integer roleId);

//    List<SysMenuMarkVo> getAllMenusByRoleIdNotHave(Integer roleId);

    /**
     * 修改角色的所拥有的菜单树
      * @param sysMenuRoleMarkModifyReq
     */
    void modifyRoleMenus(SysMenuRoleMarkModifyReq sysMenuRoleMarkModifyReq);

    /**
     * 根据用户id查询菜单树
     * @param userId
     * @return
     */
    List<SysMenuTreeVo> getAllMenusTreeByUserId(Long userId);
}
