package com.jtw.sys.mapper.information;

import com.MyMapper;
import com.github.pagehelper.Page;
import com.jtw.sys.model.information.TSysInformation;
import com.jtw.sys.vo.information.SysInformationLessVo;
import com.jtw.sys.vo.information.SysInformationVo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2019/2/21 11:54
 */
public interface SysInformationMapper extends MyMapper<TSysInformation> {
    @Select("SELECT * FROM `t_sys_information` ${whereSQL}")
    Page<TSysInformation> findAll(@Param("whereSQL") String whereSQL);

    @Select("SELECT * FROM `t_sys_information` WHERE `ID` = #{id}")
    SysInformationVo getInformationById(@Param("id") Integer id);

    @Select("SELECT * FROM `t_sys_information` ${whereSQL}")
    Page<SysInformationLessVo> findAllSysInformation(@Param("whereSQL") String whereSQL);

}
