package com.jtw.sys.service.perm;

import com.jtw.common.baseService.BaseService;
import com.jtw.sys.model.perm.TSysPerm;
import com.jtw.sys.vo.perm.SysPermTreeVo;
import com.jtw.sys.vo.perm.SysRolePermAddVo;

import java.util.List;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2018/9/12 14:02
 */
public interface SysPermService extends BaseService<TSysPerm> {

    List<String> getAllPermUrl();

    void modifyRolePerms(Integer roleId, Integer [] perms);

    List<TSysPerm> getSysRolePerms(Integer roleId);

    List<SysRolePermAddVo> getAllRolePermsByRoleIdNotHave(Integer roleId);

    /**
     * 根据用户id获取用户的权限列表
     * @param userId
     * @return
     */
    List<SysRolePermAddVo> getSysUserRolePermsByUserId(Integer userId);

    List<SysRolePermAddVo> getAllRolePermsByRoleId(Integer roleId);

    void updateSysPerm(TSysPerm sysPerm);

    List<SysPermTreeVo> getAllSysPermTreeMarkByRoleId(Integer roleId);
}
