package com.jtw.sys.vo.member;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2019/7/29 17:47
 */
@Data
public class MemberAssetRankVo implements Serializable {
    private static final long serialVersionUID = -6941921392733947092L;
    private Integer id;
    private String nickName;
    private String thumbNailImage;
    private String headImage;
    private Integer balance;
    private Integer rankNo;
    private Date updateTime;
}
