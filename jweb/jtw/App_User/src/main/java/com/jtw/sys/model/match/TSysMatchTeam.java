package com.jtw.sys.model.match;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * DESCRIPT:赛事队伍表
 *
 * @author cjsky666
 * @date 2019/7/18 10:38
 */
@Data
@Table(name = "`t_sys_match_team`")
public class TSysMatchTeam implements Serializable {
    private static final long serialVersionUID = 5486520722630522261L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID() from dual")
    private Integer id;
    private Integer matchId;
    private String name;
    private String nickName;
    private String image;
    private String ThumbNailImage;
    private String operatorIp;
    @JsonFormat(pattern="yyyy/MM/dd HH:mm:ss",timezone="GMT+8")
    private Date createTime;
    @JsonFormat(pattern="yyyy/MM/dd HH:mm:ss",timezone="GMT+8")
    private Date updateTime;
}
