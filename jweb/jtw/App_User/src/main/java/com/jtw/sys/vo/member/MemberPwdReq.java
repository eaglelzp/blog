package com.jtw.sys.vo.member;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2019/2/18 15:36
 */
@Data
public class MemberPwdReq implements Serializable {
    private static final long serialVersionUID = -8449778605710171473L;
    @NotBlank(message = "原始密码不能为空")
    private String oldPassword;
    @NotBlank(message = "新密码不能为空")
    private String password;
}
