package com.jtw.sys.util;

import com.jtw.sys.vo.task.TaskModelVo;
import lombok.extern.slf4j.Slf4j;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * DESCRIPT:计划任务执行处
 *
 * @author cjsky666
 * @date 2018/11/23 16:35
 */
@Slf4j
public class TaskFactory implements Job {
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        TaskModelVo taskModelVo = (TaskModelVo) context.getMergedJobDataMap().get("taskModelVo");
        TaskUtil.invokMethod(taskModelVo);
    }
}

