package com.jtw.sys.vo.banner;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2019/2/21 11:46
 */
@Data
public class SysBannerAddReq implements Serializable {
    private static final long serialVersionUID = -2795733622631028802L;
    private String title;
    @NotBlank(message = "原始图片不能为空")
    private String imagePath;
    @NotBlank(message = "缩略图片不能为空")
    private String thumbNailImagePath;
    private String url;
    private Integer skipWay;
    private Integer type;
    private Integer state = 0;
}
