package com.jtw.sys.service.system;

import com.github.pagehelper.Page;
import com.jtw.common.bean.enums.ReqCode;
import com.jtw.common.bean.vo.FindPagingVo;
import com.jtw.common.bean.vo.PagingVo;
import com.jtw.common.util.PagingUtil;
import com.jtw.sys.mapper.system.ConfigMapper;
import com.jtw.sys.model.system.TSysConfig;
import com.jtw.sys.vo.system.SysConfigCustomVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2019/1/18 16:27
 */
@Service
public class ConfigServiceImpl implements ConfigService {
    @Autowired
    ConfigMapper configMapper;
    @Override
    public void save(TSysConfig tSysConfig) {
            configMapper.insertSelective(tSysConfig);
    }

    @Override
    public void saveAll(List<TSysConfig> list) {

    }

    @Override
    public void update(TSysConfig tSysConfig) {
        if(!configMapper.existsWithPrimaryKey(tSysConfig.getId())){
            throw  ReqCode.UnExistingDataException.getException();
        }
        configMapper.updateByPrimaryKeySelective(tSysConfig);
    }

    @Override
    public void delete(Object id) {

    }

    @Override
    public TSysConfig getById(Object id) {
        return configMapper.selectByPrimaryKey(id);
    }

    @Override
    public Page<TSysConfig> findAll(PagingVo pagingVo) {
        return null;
    }

    @Override
    public Page<TSysConfig> findAll(FindPagingVo findPagingVo) {
        PagingUtil.start(findPagingVo);
        return  configMapper.findAll(PagingUtil.buildWhereSQLToString(findPagingVo.getFindParams()));
    }

    @Override
    public List<TSysConfig> getAll() {
        return configMapper.selectAll();
    }

    @Override
    public Map<String, String> getAllConfig() {
        Map<String,String> map = new HashMap<String, String>();
        List<SysConfigCustomVo> allConfig = configMapper.getAllConfig();
        for (SysConfigCustomVo s:allConfig) {
            map.put(s.getKeyName(),s.getKeyValue());
        }
        return map;
    }

    @Override
    public TSysConfig getByKeyName(String keyName) {
        return configMapper.getByKeyName(keyName);
    }
}
