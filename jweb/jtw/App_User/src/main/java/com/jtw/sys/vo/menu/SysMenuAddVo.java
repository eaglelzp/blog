package com.jtw.sys.vo.menu;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2018/10/10 12:02
 */
@Data
public class SysMenuAddVo extends SysMenuAddReq implements Serializable {
    private static final long serialVersionUID = -2693790469169772848L;
    @NotNull(message = "id不能为空")
    private Integer id;
}
