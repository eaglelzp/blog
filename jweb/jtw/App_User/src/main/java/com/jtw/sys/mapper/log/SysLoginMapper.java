package com.jtw.sys.mapper.log;

import com.MyMapper;
import com.jtw.sys.model.log.TSysUserLoginLog;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2018/9/21 13:06
 */
public interface SysLoginMapper extends MyMapper<TSysUserLoginLog> {
}
