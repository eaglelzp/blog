package com.jtw.sys.listener.mvcInterceptor;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.annotation.Resource;


/**
 * descript: 注册拦截器,记录访问日志
 * @author chenjian
 * @date 2018-208-22 14:28
 *
 */
@Configuration
public class MvcInterceptorConf implements WebMvcConfigurer {
    
    @Resource
    private MyInterceptor mytor;
//    @Resource
//    private MyInterceptor2 mytor2;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(mytor).addPathPatterns("/**");
//        registry.addInterceptor(mytor).addPathPatterns("/**").excludePathPatterns("/login", "/logout", "/error", "docs.html");
    }
}
