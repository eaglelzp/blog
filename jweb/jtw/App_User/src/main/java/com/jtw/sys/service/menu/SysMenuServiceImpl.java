package com.jtw.sys.service.menu;

import com.github.pagehelper.Page;
import com.jtw.common.bean.enums.ReqCode;
import com.jtw.common.bean.vo.FindPagingVo;
import com.jtw.common.bean.vo.PagingVo;
import com.jtw.common.util.PagingUtil;
import com.jtw.sys.mapper.menu.SysMenuMapper;
import com.jtw.sys.model.menu.TSysMenu;
import com.jtw.sys.vo.menu.SysMenuMarkVo;
import com.jtw.sys.vo.menu.SysMenuRoleMarkModifyReq;
import com.jtw.sys.vo.menu.SysMenuTreeVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * DESCRIPT:系统管理后台菜单服务接口实现类
 *
 * @author cjsky666
 * @date 2018/10/10 10:38
 */
@Service
@Slf4j
public class SysMenuServiceImpl implements SysMenuService {
    @Autowired
    private SysMenuMapper sysMenuMapper;

    @Override
    public void save(TSysMenu tSysMenu) {
        sysMenuMapper.insertSelective(tSysMenu);
    }

    @Override
    public void saveAll(List<TSysMenu> list) {

    }

    @Override
    public void update(TSysMenu tSysMenu) {
        sysMenuMapper.updateByPrimaryKeySelective(tSysMenu);
    }

    @Override
    public void delete(Object id) {
        if(sysMenuMapper.existsWithPrimaryKey(id)){
            sysMenuMapper.deleteByPrimaryKey(id);
        }else{
            throw ReqCode.UnExistingSysMenu.getException();
        }
    }



    @Override
    public TSysMenu getById(Object id) {
        return null;
    }

    @Override
    public Page<TSysMenu> findAll(PagingVo pagingVo) {
        PagingUtil.start(pagingVo);
        return sysMenuMapper.findAll();
    }

    @Override
    public Page<TSysMenu> findAll(FindPagingVo findPagingVo) {
        return null;
    }

    @Override
    public List<TSysMenu> getAll() {
        return sysMenuMapper.selectAll();
    }

    @Override
    public List<SysMenuMarkVo> getAllSysMenuTreeMarkByRoleId(Integer roleId) {
        return  sysMenuMapper.getAllSysMenuTreeMarkByRoleId(roleId);
    }

//    @Override
//    public List<SysMenuMarkVo> getAllMenusByRoleIdNotHave(Integer roleId) {
//        return  sysMenuMapper.getAllMenusByRoleIdNotHave(roleId);
//    }

    @Transactional
    @Override
    public void modifyRoleMenus(SysMenuRoleMarkModifyReq sysMenuRoleMarkModifyReq) {
        Set<Integer> menuId_set = new HashSet<Integer>();
        for(Integer id :sysMenuRoleMarkModifyReq.getMenus()){
            if(!sysMenuMapper.existsWithPrimaryKey(id)){
                throw ReqCode.UnExistingSysMenu.getException();
            }
            if(id>0){
                menuId_set.add(id);
            }
            filterMenuParentId(menuId_set,id);
        }
        Integer[] temp = menuId_set.toArray(new Integer[] {});
        sysMenuRoleMarkModifyReq.setMenus(temp);
        sysMenuMapper.clearRoleMenusByRoleId(sysMenuRoleMarkModifyReq.getRoleId());
        sysMenuMapper.batchSaveRoleMenusByRoleId(sysMenuRoleMarkModifyReq);
    }

    @Override
    public List<SysMenuTreeVo> getAllMenusTreeByUserId(Long userId) {
        return sysMenuMapper.getAllMenusTreeByUserId(userId);
    }


    public void filterMenuParentId(Set<Integer> set,Integer id){
        Integer parent = sysMenuMapper.getAllMenuParentIdByMenuId(id);
        if(!set.contains(parent)&&parent!=0){
            set.add(parent);
            filterMenuParentId(set,parent);
        }
    }

}
