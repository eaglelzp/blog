package com.jtw.sys.mapper.member;

import com.MyMapper;
import com.jtw.sys.model.member.TSysMemberPwdReset;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2019/2/18 17:54
 */
public interface MemberPwdResetMapper extends MyMapper<TSysMemberPwdReset> {
    /**
     * 是否有未处理的密码重置申请
     * @param userName
     * @return
     */
    @Select("SELECT * FROM `t_sys_member_pwd_reset` WHERE `USER_NAME` = #{userName} AND `STATE` = 0")
    TSysMemberPwdReset getOneByUserName(@Param("userName") Object userName);
}
