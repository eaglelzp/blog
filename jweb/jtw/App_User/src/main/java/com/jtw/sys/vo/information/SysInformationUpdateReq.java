package com.jtw.sys.vo.information;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2019/2/21 11:46
 */
@Data
public class SysInformationUpdateReq implements Serializable {
    private static final long serialVersionUID = 6946742978200193332L;
    @NotNull(message = "id不能为空")
    private Integer id;
    private String author;
    private String title;
    @NotNull(message = "审核状态不能为空")
    private Integer state;
    @NotBlank(message = "资讯内容不能为空")
    private String content;
    private String descript;
}
