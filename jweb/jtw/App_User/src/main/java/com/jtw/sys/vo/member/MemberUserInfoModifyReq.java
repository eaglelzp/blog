package com.jtw.sys.vo.member;

import com.jtw.sys.vo.user.SysUserInfoModifyReq;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * DESCRIPT:
 *
 * @author cjsky666
 * @date 2019/2/25 9:37
 */
@Data
public class MemberUserInfoModifyReq extends SysUserInfoModifyReq implements Serializable {
    private static final long serialVersionUID = -367969968366051121L;
    @NotBlank(message = "昵称不能为空")
    private String nickName;
    private String headImage;
    private Integer sex;
    private String thumbNailImage;
    private Integer age;
    private String mobile;
    private String email;
    private String qq;
    private String address;
    private String desc;
}
