package com.jtw.sys.vo.menu;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * DESCRIPT:用户管理菜单树
 *
 * @author cjsky666
 * @date 2018/10/24 22:38
 */
@Data
public class SysMenuTreeVo implements Serializable {
    private static final long serialVersionUID = 5321895723848265584L;
    private Integer id;
    private String name;
    private String path;
    private Integer type;
    private List<SysMenuMarkVo> children;
}
