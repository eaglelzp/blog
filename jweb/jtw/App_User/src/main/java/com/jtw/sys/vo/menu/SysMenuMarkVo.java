package com.jtw.sys.vo.menu;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * DESCRIPT: 角色拥有的菜单树并且标记
 *
 * @author cjsky666
 * @date 2018/10/22 11:22
 */
@Data
public class SysMenuMarkVo implements Serializable {
    private static final long serialVersionUID = 3434897845198201778L;
    private Integer id;
    private String name;
    private Integer type;
    private Boolean mark;
    private List<SysMenuMarkVo> children;
}
