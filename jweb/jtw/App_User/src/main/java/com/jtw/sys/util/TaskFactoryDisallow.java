package com.jtw.sys.util;

import com.jtw.sys.vo.task.TaskModelVo;
import lombok.extern.slf4j.Slf4j;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * DESCRIPT:若一个方法一次执行不完下次轮转时则等待改方法执行完后才执行下一次操作
 *
 * @author cjsky666
 * @date 2018/11/23 16:45
 */
@DisallowConcurrentExecution
@Slf4j
public class TaskFactoryDisallow implements Job {

    public void execute(JobExecutionContext context) throws JobExecutionException {
        TaskModelVo taskModelVo = (TaskModelVo) context.getMergedJobDataMap().get("taskModelVo");
        TaskUtil.invokMethod(taskModelVo);
    }
}