var openurl = {
  getLoginCaptcha:"sys/captcha/getLoginCaptcha",
  login:"sys/member/login",
  sign:"sys/member/sign",
  logout:"sys/member/logout",
  getAllSysConfig:"sys/config/getAllSysConfig",
  resetMemberPwd:"sys/member/resetMemberPwd",
  getInformationById:"sys/information/getInformationById",
  getAllMobileClientBanner:"sys/config/getAllMobileClientBanner",
  getAllClientStatistics:"sys/config/getAllClientStatistics",
  getAllHistoryWins:"sys/member/getAllHistoryWins",
}

var gameUrl = {
  getAllOpened:"sys/member/game/getAllOpened",
  getAllOpenedOfGroup:"sys/member/game/getAllOpenedOfGroup",


  getMemberGameVoById:"sys/member/game/getMemberGameVoById",
  getMemberCurrentGameTimeByGameId:"sys/member/game/getMemberCurrentGameTimeByGameId",
  getGameOpenCodeByGameIdAndExpect:"sys/member/game/getGameOpenCodeByGameIdAndExpect",
  getHistoryDataByGameId:"sys/member/game/getHistoryDataByGameId",
}
var memberAssetUrl = {
  getMyAsset:"sys/member/asset/getMyAsset",
  findAllMyAssetLog:"sys/member/asset/findAllMyAssetLog",
  getAllSysBanks:"sys/member/asset/getAllSysBanks",
  getAllMyBanks:"sys/member/asset/getAllMyBanks",
  rechargeBalance:"sys/member/asset/rechargeBalance",
  findAllMyRechargeLog:"sys/member/asset/findAllMyRechargeLog",
  withdrawBalance:"sys/member/asset/withdrawBalance",
  exchangeCoinToCash:"sys/member/asset/exchangeCoinToCash",
  findAllMyWithdrawBalanceLog:"sys/member/asset/findAllMyWithdrawBalanceLog",
  addMemberBank:"sys/member/asset/addMemberBank",
  modifyMemberBank:"sys/member/asset/modifyMemberBank",
}

var memberUrl ={
  getMemberUserBaseInfoVo:"sys/member/getMemberUserBaseInfoVo",
  modifyMemberPwd:"sys/member/modifyMemberPwd",
  modifyMemberWithdrawPwd:"sys/member/modifyMemberWithdrawPwd",
  resetMemberWithdrawPwd:"sys/member/resetMemberWithdrawPwd",
  getMemberAgentInfo:"sys/member/getMemberAgentInfo",
  bottomAgentApply:"sys/member/bottomAgentApply",
  getMemberInviteCode:"sys/member/getMemberInviteCode",
  getAllMemberRecommend:"sys/member/getAllMemberRecommend",
  findAllTeamMember:"sys/member/findAllTeamMember",
  findAllTeamMemberBetByUserId:"sys/member/findAllTeamMemberBetByUserId",
  findAllTeamMemberAssetLog:"sys/member/findAllTeamMemberAssetLog",
  getMemberProfitAndLoss:"sys/member/getMemberProfitAndLoss",
  findAllTeamMemberProfitAndLoss:"sys/member/findAllTeamMemberProfitAndLoss",
  getMemberTeamStatistics:"sys/member/getMemberTeamStatistics",
  modifyMemberUserBaseInfo:"sys/member/modifyMemberUserBaseInfo",
  addAgentBottomLevel:"sys/member/addAgentBottomLevel",
  addAgentMiddleLevel:"sys/member/addAgentMiddleLevel",
  addAgentTeamMember:"sys/member/addAgentTeamMember",
  findAllMySubAgent:"sys/member/findAllMySubAgent",
  findAllMyTeamReward:"sys/member/findAllMyTeamReward",
  findAllTeamRewardAgent:"sys/member/findAllTeamRewardAgent",
  findAllMyTeamRewardDetail:"sys/member/findAllMyTeamRewardDetail",
  findAllTeamRewardAgentDetail:"sys/member/findAllTeamRewardAgentDetail",
  findAllTeamRewardAgentDetailInfo:"sys/member/findAllTeamRewardAgentDetailInfo",
  findAllSysInformation:"sys/member/findAllSysInformation",
  findAllOpenGameRule:"sys/member/findAllOpenGameRule",
  getSubAgentTeamStatistics:"sys/member/getSubAgentTeamStatistics",
  findAllDayRewardStatistics:"sys/member/findAllDayRewardStatistics",
  findAllDayRewardStatisticsBet:"sys/member/findAllDayRewardStatisticsBet",
  getTodayCheckIn:"sys/member/getTodayCheckIn",
  memberCheckIn:"sys/member/memberCheckIn",
  findAllCheckInReward:"sys/member/findAllCheckInReward",
}
var memberbetUrl = {
  saveBuyBet:"sys/member/bet/saveBuyBet",
  cancelBuyBet:"sys/member/bet/cancelBuyBet",
  findAllMemberBetByUserId:"sys/member/bet/findAllMemberBetByUserId",
  getAllCurrentGameTimeBetsDataByGameIdAndExcept:"sys/member/bet/getAllCurrentGameTimeBetsDataByGameIdAndExcept",
}
var fileUrl ={
  uploadSingleHeadImage:"sys/member/uploadSingleHeadImage",
}

var URLS={};
var interfaces={openurl,gameUrl,memberAssetUrl,memberUrl,memberbetUrl,fileUrl};

for(var k in interfaces){
  for(var j in interfaces[''+k+'']){
    URLS[''+j+'']=interfaces[''+k+''][''+j+''];
  }
}
console.log(URLS);

export default URLS;
