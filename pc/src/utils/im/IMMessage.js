/**
 * 通信内容包装类--- 客户端
 * @param rid 接收者
 * @param sid 发送者
 * @param ctx 主要内容Object
 * @param rname 接收者昵称
 * @param sname 发送者昵称
 * @constructor 构造器
 */
function IMMessage(rid,sid,ctx,rname,sname){
  console.log(rid,sid,ctx,rname,sname);
  this.rid= rid;
  this.sid= "c"+sid;
  this.ctx= ctx;
  this.rname= rname;
  this.sname= sname;
}

IMMessage.prototype = {
  constructor:IMMessage
}

export default IMMessage;
