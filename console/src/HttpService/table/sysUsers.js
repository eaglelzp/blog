// view_list_sysUsers.vue
export default  {
  userName: {
    params: {
      value: "",
      column: "userName",
      condition: "LIKE",
      prefix: ""
    },
    orderBy: {
      state: 0,
      index: 0,
      value: '',
      options: []
    },
    alias: '包含',
    state: 0,
    label: "用户名",
    type: 1,//输入框类型
    conditions: {
      options: [
        {opt: "LIKE", alias: "包含"},
        {opt: "=", alias: "等于"},
      ]
    }
  },
  mobile: {
    params: {
      value: "",
      column: "mobile",
      condition: "LIKE",
      prefix: ""
    },
    orderBy: {
      state: 0,
      index: 0,
      value: '',
      options: []
    },
    alias: '包含',
    state: 0,
    label: "手机号",
    type: 1,
    conditions: {
      options: [
        {opt: "LIKE", alias: "包含"},
        {opt: "=", alias: "等于"},
      ]
    }
  },
  nickName: {
    params: {
      value: "",
      column: "nickName",
      condition: "LIKE",
      prefix: ""
    },
    orderBy: {
      state: 0,
      index: 0,
      value: '',
      options: []
    },
    alias: '包含',
    state: 0,
    label: "昵称",
    type: 1,
    conditions: {
      options: [
        {opt: "LIKE", alias: "包含"},
        {opt: "=", alias: "等于"}
      ]
    }
  },
  createTime: {
    params: {
      column: "createTime"
    },
    orderBy: {
      state: 1,
      value: 'desc',
      options: [
        {opt: 'desc', alias: '降序'},
        {opt: 'asc', alias: '升序'}
      ]
    },
    label: "注册时间",
    type: 2,//时间筛选类型
    filter: [
      {
        params: {
          value: "",
          column: "createTime",
          condition: ">=",
          prefix: "`t_sys_user_info`.",
        },
        alias: '大于等于',
        state: 0,
        value: "00:00:00",
        label: "注册开始时间",
        conditions: {
          options: [
            {opt: ">=", alias: "大于等于"},
            {opt: "=", alias: "等于"},
            {opt: ">", alias: "大于"}
          ]
        }
      },
      {
        params: {
          value: "",
          column: "createTime",
          condition: "<=",
          prefix: "`t_sys_user_info`.",
        },
        alias: '小于等于',
        state: 0,
        label: "注册结束时间",
        value: "23:59:59",
        conditions: {
          options: [
            {opt: "<=", alias: "小于等于"},
            {opt: "=", alias: "等于"},
            {opt: "<", alias: "小于"}
          ]
        }
      }
    ]
  },
  age: {
    params: {
      value: "",
      column: "age",
      condition: "=",
      isString: false,
      prefix: ""
    },
    orderBy: {
      state: 0,
      index: 0,
      value: '',
      options: []
    },
    alias: '等于',
    state: 0,
    label: "年龄",
    type: 1,
    conditions: {
      options: [
        {opt: "=", alias: "等于"},
        {opt: ">", alias: "大于"},
        {opt: "<", alias: "小于"},
        {opt: ">=", alias: "大于等于"},
        {opt: "<=", alias: "大于等于"}
      ]
    }
  },
  address: {
    params: {
      value: "",
      column: "address",
      condition: "LIKE",
      prefix: ""
    },
    orderBy: {
      state: 0,
      index: 0,
      value: '',
      options: []
    },
    alias: '包含',
    state: 0,
    label: "地址",
    type: 1,
    conditions: {
      options: [
        {opt: "LIKE", alias: "包含"}
      ]
    }
  },
  sex: {
    params: {
      value: "",
      column: "sex",
      condition: "=",
      isString: false,
      prefix: ""
    },
    orderBy: {
      state: 0,
      index: 0,
      value: '',
      options: []
    },
    alias: "",
    state: 0,
    label: "性别",
    type: 3,//下拉选择类型
    conditions: {
      options: [
        {opt: "1", alias: "男"},
        {opt: "2", alias: "女"},
        {opt: "0", alias: "保密"}
      ]
    }
  },
  state: {
    params: {
      value: "",
      column: "state",
      isString: false,
      condition: "=",
      prefix: "`t_sys_user`.",
    },
    orderBy: {
      state: 0,
      index: 0,
      value: '',
      options: []
    },
    alias: "",
    state: 0,
    label: "账号状态",
    type: 3,//下拉选择类型
    conditions: {
      options: [
        {opt: "false", alias: "正常"},
        {opt: "true", alias: "锁定"}
      ]
    }
  }
};
